# Tarefa de classificacão

Aluno: Matheus de Sousa Lemos Fernandes
Matrícula: 16/0137969  
Mineracão de Textos - UnB - 2019/1  

## Resumo
A tarefa consistia de utilizar diferentes classificadores para classificar um conjunto de textos em diferentes datasets.


## Etapas

As etapas do programa em Python consistem de:

* Carregar o dataset
* Limpar os documentos de texto (com cleaning, stemming e tfidf)
* Classificar o dataset usando 3 algoritmos (Multinomial Naive-Bayes, SVM e KNN)

O programa itera por todos os datasets e executa o programa de classificacão em python para cada dataset.  
Cada iteracão gera o score da classificacão, acuracia, precisão, revocacão e F1-score de cada algoritmo para o dado dataset.  
Os dados são escritos no stdout a medida que são executados e por fim todos os dados são escritos em um arquivo *scoring.csv*  
Para os testes de classificacão foi usada a estratégia **Holdout** de avaliacão.  

## Execucão

É necessário que a pasta dos datasets esteja no mesmo diretório que o arquivo main.sh . O arquivo datasets.zip deve ser descompactado nesse mesmo diretório. 
Após descompactados, para rodar o programa basta executar o comando:

```
./main.sh
```

É importante frisar que os dados são escritos no arquivo scoring.csv ao fim da execucão, então é necessário esperar a execucão completa.

## Formatacão

Os dados são formatados em um arquivo .csv, onde são colocados o nome do dataset, a acurácia, precisão, revocacão e f1-score, para cada um dos classificadores.
