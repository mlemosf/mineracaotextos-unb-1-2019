#!/bin/bash

# Roda os algoritmos de classificacão de texto 
# nos diferentes datasets
# Matheus Lemos <matheuslemosf@protonmail.com> 1-2019
# Matrícula: 16/0137969

dir=$(pwd)
datasets=$(ls $dir/datasets)
filename='scoring.csv'

# headers='datasets,mnb_score,mnb_cm,mnb_acuracia, mnb_precisao, mnb_revocacao, mnb_fscore, 
# linear_svm_score,linear_svm_cm,linear_svm_acuracia,linear_svm_precisao,linear_svm_revocacao,linear_svm,fscore,
# knn_score,knn_cm,knn_acuracia,knn_precisao,knn_revocacao,knn_fscore'

headers='datasets,
mnb_score,linear_svm_score,knn_score,
mnb_acuracia, linear_svm_acuracia, knn_acuracia,
mnb_revocacao, linear_svm_revocacao, knn_revocacao,
mnb_precisao, linear_svm_precisao, knn_precisao,
mnb_f1score, linear_svm_f1score, knn_f1score'

echo "=> CLASSIFICANDO"
for item in $datasets; do
	full_item="datasets/$item"
	out=$(echo $item | awk '{print tolower($0)}')
	out="$out,""$(python main.py $full_item)" 
	echo "$out"
	result="$result"$'\n'"$out"
done

echo "=> GERANDO CSV"

echo $headers > $filename
echo "$result" >> $filename

echo "=> ENCERRANDO"
