from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
import string
from nltk.stem.porter import PorterStemmer
from util import Loader

class Prep:
	

	def dummy_fun(self, doc):
		return doc

	# Cleaning

	def clean(self, x):
		x_clean = []
		for item in x:
			x_clean.append(item.translate(str.maketrans('', '', string.punctuation)))
		return x_clean

	# Stemming 

	def stem(self, x):
		porter_stemmer = PorterStemmer()
		i = 0
		tokens = []
		temp = []
		for item in x:
			tokenized = nltk.word_tokenize(x[i]) 
			temp = []
			for word in tokenized:
				temp.append(porter_stemmer.stem(word))
			tokens.append(temp)
			i+=1 
		return tokens

	# Tfidf

	def tfidf(self, x):
		vect = TfidfVectorizer(max_df=0.90, min_df=0.01,analyzer='word', tokenizer=self.dummy_fun, preprocessor=self.dummy_fun, token_pattern=None)
		x_train = vect.fit(x)
		x_tfidf = vect.transform(x)
		return x_tfidf

	def prep_text(self, x):
		x_clean = self.clean(x)
		x_stem = self.stem(x_clean)
		result = self.tfidf(x_stem)
		return result

# Carrega o dataset

def load_file(filename):
	loader = Loader()
	corpus = loader.from_files(filename)
	x = corpus['corpus']
	y = corpus['class_index']
	return x, y
