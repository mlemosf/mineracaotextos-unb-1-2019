from prep import Prep
from prep import load_file
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import classifiers
import sys
import csv

# Arquivo do dataset a ser utilizado
filename = sys.argv[1]
x,y = load_file(filename)

# Preparar os dados de teste antes da classificacão (stemming, cleaning, tfidf)
prep = Prep()
prepped = prep.prep_text(x)

results = []

# Testes usando a estratégia 'Holdout'
X_train, X_test, y_train, y_test = train_test_split(prepped,y, test_size=0.3, random_state=0)

# Classificacão usando 3 classificadores (MultinomialNB, SVC e KNN) e matriz de confusão

result1, score1 = classifiers.mnbClassifier(X_train, X_test,y_train, y_test)
result2, score2 = classifiers.svmClassifier(X_train, X_test,y_train, y_test)
result3, score3 = classifiers.knnClassifier(X_train, X_test,y_train, y_test)

results.append(score2)
results.append(score1)
results.append(score3)


# Matriz de confusão 
# results.append(classifiers.cm(y_test, result1))
# results.append(classifiers.cm(y_test, result2))
# results.append(classifiers.cm(y_test, result3))

# Medidas de desempenho
results.append(classifiers.acuracia(y_test, result1))
results.append(classifiers.acuracia(y_test, result2))
results.append(classifiers.acuracia(y_test, result3))

results.append(classifiers.revocacao(y_test, result1))
results.append(classifiers.revocacao(y_test, result2))
results.append(classifiers.revocacao(y_test, result3))

results.append(classifiers.precisao(y_test, result1))
results.append(classifiers.precisao(y_test, result2))
results.append(classifiers.precisao(y_test, result3))

results.append(classifiers.f1score(y_test, result1))
results.append(classifiers.f1score(y_test, result2))
results.append(classifiers.f1score(y_test, result3))

print(','.join(map(str,results)))