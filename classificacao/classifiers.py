from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.svm import SVC
import numpy as np



# Classificadores

def mnbClassifier(x_train, x_test, y_train, y_test):
	clf = MultinomialNB()
	clf.fit(x_train, y_train)
	result = clf.predict(x_test)
	x_score = clf.score(x_test,y_test)
	return result, x_score


def svmClassifier(x_train, x_test, y_train, y_test):
	clf = SVC(gamma='auto', kernel='linear')
	clf.fit(x_train, y_train)
	result = clf.predict(x_test)
	x_score = clf.score(x_test,y_test)
	return result, x_score

def knnClassifier(x_train, x_test, y_train, y_test):
	clf = KNeighborsClassifier(n_neighbors=3)
	clf.fit(x_train, y_train)
	result = clf.predict(x_test)
	x_score = clf.score(x_test,y_test)
	return result, x_score


# Matriz de confusão 

def cm(y_train, y_test):
	np.set_printoptions(linewidth=np.inf)
	return (confusion_matrix(y_train, y_test)).tolist()

# Avaliar a performance

def acuracia(y_train, y_test):
	return accuracy_score(y_train, y_test)

def precisao(y_train, y_test):
	return precision_score(y_train, y_test, average='micro', labels=np.unique(y_test))

def revocacao(y_train, y_test):
	return recall_score(y_train, y_test, average='micro', labels=np.unique(y_test))

def f1score(y_train, y_test):
	return f1_score(y_train, y_test, average='micro', labels=np.unique(y_test))
